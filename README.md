## A small number of simple scripts that I use daily on my Thinkpad X220 running Debian GNU/Linux 11 (Bullseye)

All the scripts and their uses are documentations are linked below in no specific order

- [cpubar](https://codeberg.org/monochrome/Scripts/src/branch/main/cpubar)
- [logout-wofi](https://codeberg.org/monochrome/Scripts/src/branch/main/logout-wofi)
- [screencast](https://codeberg.org/monochrome/Scripts/src/branch/main/screencast)
- [screenshot](https://codeberg.org/monochrome/Scripts/src/branch/main/screenshot)
- [setbg](https://codeberg.org/monochrome/Scripts/src/branch/main/setbg)
- [shuffbg](https://codeberg.org/monochrome/Scripts/src/branch/main/shuffbg)
- [usage](https://codeberg.org/monochrome/Scripts/src/branch/main/usage)
- [wofi-wifi](https://codeberg.org/monochrome/Scripts/src/branch/main/wofi-wifi)
- [zipall](https://codeberg.org/monochrome/Scripts/src/branch/main/zipall)
