# Simple script to set wallpaper in sway

This script has 3 options
- set a specific file as your wallpaper 
- randomly pick a wallpaper from `$HOME/Pictures/Wallpapers`
- set the current phase of the moon as the wallpaper using `pngphoon`

The commands are as follows:
```
Usage: setbg [-s] [-x] [-h] [filepath]
Options:
-s : Shuffles your wallpaper and sets one from it
-x : Sets the current phase of the moon as wallpaper
-h : Shows this fucking help menu
To set a specific file, use no flags and provide path
```

## Dependancy

- `swaybg`
- `pngphoon`
