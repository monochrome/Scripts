# Simple script to zip all the directories in a directory with its name

Lets say there is a dir with dirs a,b,c,d. After `zipall` is run, there will be a.zip, b.zip, c.zip and d.zip.

## Dependancy

- `zip`
