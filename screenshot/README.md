# screenshot

A simple script to take screenshots. 

The script does 2 things.
- Takes a screenshot and saves it in `$HOME/Pictures/Screenshots/`
- copies the screenshot on your clipboard

The script can be used in 2 ways.
- CLI
- Wofi

The commands are as follows:
```
Usage: screenshot [-f] [-s] [-w] [-h]
Options:
-f : Fullscreen screenshot
-s : Select a region for the screenshot
-w : Shows a wofi prompt for screenshot
-h : Shows this fucking help menu
If no flags are selected then -f is chosen
```

## Dependancies

- `grim` for taking the screenshot
- `slurp` for selecting a region
- `wl-clipboard` provides the command `wl-copy` which is used to for copy to clipboard 
