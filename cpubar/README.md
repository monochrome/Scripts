# CPUbar
## (Stolen from Luke Smith)

CPUbar, as the name suggests is a script that shows you the cpu load on your system, this is a script that I use inside waybar.

Add the following to your waybar/config file
```
	"custom/cpubar": {
		"exec": "<path to cpubar script>",
		"format": "CPU {}",
		"interval": 1,
		"tooltip": false,
	},
```
Also add in `"custom/cpubar"` to `modules-{left/right/center}` to see it in left,right or center

(If you dont use waybar, then check your bar's documentation on how to add shellscripts.)

[demo video](https://codeberg.org/monochrome/Scripts/src/branch/main/cpubar/cpubar.mp4)
