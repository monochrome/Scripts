# screencast

A simple script to record your screen using `wf-recorder`.
The script can be used in 2 ways.
- CLI
- Wofi
The commands are as follows
```
Usage: screencast [-f] [-s] [-w] [-h]
Options:
-f : Fullscreen screencast
-a : Fullscreen screencast with audio
-s : Select a region for the screencast
-w : Shows a wofi prompt for screencast
-h : Shows this fucking help menu
```

## Dependancies

- `wf-recorder`
- `notify-send` to send you a notification
- `wofi` to show a wofi menu to pick an option
